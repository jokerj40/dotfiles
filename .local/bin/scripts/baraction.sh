#!/bin/bash

# for spectrwm

print_layout() {
    lyt=$(xset -q|grep LED| awk '{ print $10 }')
    if [[ "$lyt" -eq 00000000 ]]; then
        echo -n " us";
    elif [[ "$lyt" -eq 00001000 ]]; then
        echo -n " ru";
    fi
}

print_battery() {
    cap=$(cat /sys/class/power_supply/BAT1/capacity)
    cap="${cap}%"
    status=$(cat /sys/class/power_supply/BAT1/status)
    if [[ "$status" = "Charging" ]]; then
        cap="${cap}+";
    fi
    echo -n "| bat: $cap "
}


while :; do
    print_battery;
    print_layout
    echo ""
    sleep 0.45
done
