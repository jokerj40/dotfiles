#!/usr/bin/bash

docker run -d --name soulseek --restart=unless-stopped \
-v "$HOME/loads/torrents/soulseek/appdata":"/data/.SoulseekQt" \
-v "$HOME/loads/torrents/soulseek/loads":"/data/Soulseek Downloads" \
-v "$HOME/loads/torrents/soulseek/logs":"/data/Soulseek Chat Logs" \
-v "$HOME/loads/torrents/soulseek/shared":"/data/Soulseek Shared Folder" \
-e PGID=1000 \
-e PUID=1000 \
-p 6080:6080 \
realies/soulseek
