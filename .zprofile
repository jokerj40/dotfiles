#!/bin/zsh

# Set up PATH
export PATH="$PATH:$(du "$HOME/.local/bin" | cut -f2 | paste -sd ':')"
# Nix PATH
export PATH="$PATH:$HOME/.nix-profile/bin"

# Default programs:
export EDITOR="nvim"
export BROWSER="firefox"
export FILEMANAGER='nemo'

# XDG
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_DESKTOP_DIR="$HOME"

# Clean up
export PYLINTHOME="$XDG_CACHE_HOME/pylint"
export JULIA_DEPOT_PATH="$XDG_DATA_HOME/julia:$JULIA_DEPOT_PATH"
export MAXIMA_USERDIR="$XDG_CONFIG_HOME/maxima"
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/pythonrc"
export PYTHON_HISTORY="$XDG_STATE_HOME/python_history"
export PYTHONPYCACHEPREFIX="$XDG_CACHE_HOME/python"
export PYTHONUSERBASE="$XDG_DATA_HOME/python"
export LESSHISTFILE="-"
#export XAUTHORITY="/run/user/$(id -u)/Xauthority" # for voidlinux
export ICEAUTHORITY="$XDG_CACHE_HOME/ICEauthority" # This line will break some DMs
export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority" # This one too
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export GOPATH="$XDG_DATA_HOME/go"
export GRASSDIR="$HOME/.grass7"
export DOT_SAGE="$XDG_CONFIG_HOME/sage"
export TEXMFHOME="$XDG_DATA_HOME/texmf"
export TEXMFVAR="$XDG_CACHE_HOME/texlive/texmf-var"
export TEXMFCONFIG="$XDG_CONFIG_HOME/texlive/texmf-config"
export TMUX_TMPDIR="$XDG_RUNTIME_DIR"
export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
export WGETRC="${XDG_CONFIG_HOME:-$HOME/.config}/wget/wgetrc"
export SQLITE_HISTORY="$XDG_CACHE_HOME/sqlite_history"
export WEECHAT_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/weechat"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export DOCKER_CONFIG="$XDG_CONFIG_HOME/docker"
export AWS_SHARED_CREDENTIALS_FILE="$XDG_CONFIG_HOME/aws/credentials"
export AWS_CONFIG_FILE="$XDG_CONFIG_HOME/aws/config"
export VAGRANT_ALIAS_FILE="$XDG_DATA_HOME/vagrant/aliases"
export VAGRANT_HOME="$XDG_DATA_HOME/vagrant"
export ANSIBLE_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/ansible/ansible.cfg"
export ELINKS_CONFDIR="$XDG_CONFIG_HOME/elinks"
export TF_PLUGIN_CACHE_DIR="$XDG_CACHE_HOME/terraform/plugin-cache"
export TF_LOG_PATH=./terraform.log
export TF_CLI_CONFIG_FILE="$XDG_CONFIG_HOME/terraform/.terraformrc"
export FZF_DEFAULT_OPTS='--bind ctrl-j:down,ctrl-k:up --layout=reverse --height 40%'
export R_LIBS_USER="~/.local/share/R/Library"

export USERXSESSION="$XDG_CACHE_HOME/X11/xsession"
export USERXSESSIONRC="$XDG_CACHE_HOME/X11/xsessionrc"
export ALTUSERXSESSION="$XDG_CACHE_HOME/X11/Xsession"
export ERRFILE="$XDG_CACHE_HOME/X11/xsession-errors"

if [ ! -d "$HOME/.cache/mycli" ]; then
    mkdir -p $HOME/.cache/mycli
fi
export MYCLI_HISTFILE="$HOME/.cache/mycli/mycli-history"
if [ ! -d "$HOME/.cache/zsh" ]; then
    mkdir -p $HOME/.cache/zsh
fi
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"

# export GNUPGHOME="$XDG_DATA_HOME/gnupg" # tinkering with systemd required

# Less flags
export LESS=-Ri # colored output & insensitive search
export LESSOPEN="| /usr/bin/highlight -O ansi %s 2>/dev/null"

# Man pager
# Vim as manpager
export MANPAGER='nvim +Man!'
export MANWIDTH=120
# Coloring for man
export GROFF_NO_SGR=1 # or export MANROFFOPT=-c


# Start MPD server
# [ ! -s ~/.config/mpd/pid ] && mpd

# Start X server
if [[ "$(tty)" = "/dev/tty1" ]]; then
    startx
fi

# Requires actual sudo :(
sudo -n loadkeys ${XDG_DATA_HOME:-$HOME/.local/share}/keymap/tty.kmap 2>/dev/null
