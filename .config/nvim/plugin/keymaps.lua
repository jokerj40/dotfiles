local set = vim.keymap.set

set('n', '<leader>x', '<cmd>.lua<CR>', { desc = 'Execute the current line' })
set('n', '<leader><leader>x', '<cmd>source %<CR>', { desc = 'Execute the current file' })

-- neovim nightly would fix this
set('n', 'grn', vim.lsp.buf.rename)
set('n', 'gra', vim.lsp.buf.code_action)
set('n', 'grr', vim.lsp.buf.references)

-- formatting
-- vim.keymap.set('n', '<leader>f', function() vim.lsp.buf.format() end)

-- split navigation
set('n', '<c-h>', '<c-w>h')
set('n', '<c-j>', '<c-w>j')
set('n', '<c-k>', '<c-w>k')
set('n', '<c-l>', '<c-w>l')
-- split resizing
set("n", "<m-,>", "<c-w>5>")
set("n", "<m-.>", "<c-w>5<")
set("n", "<m-t>", "<c-w>+")
set("n", "<m-s>", "<c-w>-")

set('n', '<leader>C', ':let @/=\'\'<CR>', { desc = 'Remove search highlight' })
