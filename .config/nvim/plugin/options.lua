local opt = vim.opt

-- visual line for 120 char limit
opt.colorcolumn = '120'
opt.signcolumn = 'yes'

-- disable swapfile
opt.swapfile = false

-- disable neovim message
opt.shortmess:append('I')

-- relative numbers
opt.number = true
opt.relativenumber = true

-- search settings
opt.inccommand = "split"

opt.smartcase = true
opt.ignorecase = true

-- intendation settings
opt.tabstop = 8       -- number of visual spaces per TAB
opt.softtabstop = 0   -- number of spaces in tab when editing
opt.expandtab = true  -- tabs are space
opt.shiftwidth = 4    -- number of spaces to use for autoindent
opt.autoindent = true -- set smarttab
opt.copyindent = true -- copy indent from the previous line

-- better case-sensitivity when searching
opt.ignorecase = true
opt.smartcase = true

opt.wrap = false -- disable word wrap

-- :sp splits the bottom and :vsp splits to the right
opt.splitbelow = true
opt.splitright = true

-- encoding
opt.encoding = 'utf-8'
opt.fileencoding = 'utf-8'
opt.fileencodings = 'utf-8'
