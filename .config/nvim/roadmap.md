# add these:
+ telescope + addons (fd, fzf)
+ plenary (as telescope dep)
+ mini.nvim (for nice-to-haves, like surround, autopairs and jumps ?)
    > an alternative to 'f': leap.nvim

* neogit/vgit for changed lines
* highlight and delete whitespaces (trim.nvim)
* snacks.nvim
* nvim-dap, formatter like conform.nvim
* lsp-status.nvim for diagnostics
* Suda.vim or link neovim config to root/make an alias sudo -E nvim -u /path/to/config
* lines for brackets (indent-blankline.nvim or snacks.nvim), colored brackets (chech rainbow delimiters.nvim)
* trainers (like for nvim-surround), debuggers for neovim itself and for external langs, inspectors for config & hotkeys
* mini.clue
* LuaSnip for LaTeX snippets
* kanagawa ->  tokyonight.nvim
* trouble.nvim from folke for better diagnostics window

# done:
+ treesitter
+ nvim-lsp
    > an alternative: install LSPs with mason.nvim + mason-lsp-config
+ lazydev.nvim
+ blink-cmp
    > an alternative: nvim-cmp

# todo:
+ lualine component instead of "fidget.nvim"
+ fix icons, one set for each plugin
+ completion only when I explicitly tell to autocomplete
+ separate plugins to their files, minimal init.lua, divide and rule. (lua/custom, move lazy to init.lua, plugin, after/{ft,}plugin)
+ line with lsp statistics
+ change font
+ explore intsalled plugins defaults
+ code formatting, commenting, indenting
+ configure lualine
+ dig into LLS-Addons (love2d, awesome). Something similar TJ has in his config.awesome repo
+ highlight when yanking text
+ look for keybindings in TJ's repo and in famous neovimers configs
+ toggle enable/disable completion, and when disabled, be able to call for completion once
+ write a 'plugin' that 'fixes' man formatting and adds color to bold/italics text or generates it from LESS_.* env variables
