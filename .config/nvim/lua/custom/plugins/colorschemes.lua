return {
    'rebelot/kanagawa.nvim',
    opts = {
        commentStyle = { italic = false }
    },
    config = function()
        vim.cmd('colorscheme kanagawa')
    end
}
