return {
    'neovim/nvim-lspconfig',
    dependencies = {
        {
            'saghen/blink.cmp',
            dependencies = 'rafamadriz/friendly-snippets',
            version = '*',
            -- {{ this opts table could be smaller
            opts = {
                keymap = { preset = 'default' },

                appearance = {
                    use_nvim_cmp_as_default = true,
                    -- nerd_font_variant = 'mono'
                },
                sources = {
                    default = { 'lazydev', 'lsp', 'path', 'snippets', 'buffer' },
                    providers = {
                        lazydev = {
                            name = 'LazyDev',
                            module = 'lazydev.integrations.blink',
                            score_offset = 100,
                        },
                    },
                },
                signature = { enabled = true },
            },
            opts_extend = { 'sources.default' }
            --}}
        },
        {
            'folke/lazydev.nvim',
            opts = {
                library = {
                    '/usr/share/lua',
                    vim.env.HOME .. '/.config/awesome',
                    vim.env.HOME .. '/Desktop/dev/lua/love/definitions',
                    { path = '/usr/share/awesome/lib', words = { 'awesome', 'awful' } },
                    { path = '${3rd}/luv/library',     words = { 'vim%.uv' } },
                },
            },
        },
        { "j-hui/fidget.nvim", opts = {} },
    },
    config = function()
        local capabilities = require('blink.cmp').get_lsp_capabilities()
        local lspconfig = require('lspconfig')
        lspconfig.lua_ls.setup {
            settings = {
                Lua = {
                    diagnostics = {
                        enable = true,
                        disable = { 'lowercase-global', 'unused-local' },
                        globals = {
                            'awesome',
                            'client',
                            'screen',
                            'root',
                        },
                    },
                    telemetry = {
                        enable = false,
                    },
                }
            },
            capabilities = capabilities,
        }

        lspconfig.clangd.setup {
            capabilities = capabilities,
        }

        lspconfig.gopls.setup {
            capabilities = capabilities,
        }
    end,
}
