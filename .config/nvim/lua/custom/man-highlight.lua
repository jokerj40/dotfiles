local M = {}

local defaults = {}

local function prepare_table(opts)
    if opts == nil then return end
    local result = {}
    for k,v in pairs(opts) do
        result[k] = v
    end

    return result
end

local function apply_hsettings(man_hsettings)
    if man_hsettings == nil then return end
    for k,v in pairs(man_hsettings) do
        vim.api.nvim_set_hl(0, k, v)
    end
end

M.setup = function(opts)
    opts = vim.tbl_deep_extend("force", defaults, opts or {})
    man_hsettings = prepare_table(opts)
    apply_hsettings(man_hsettings)
end

return M
