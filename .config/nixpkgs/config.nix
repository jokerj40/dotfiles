{
    packageOverrides = pkgs: with pkgs; {
        myPackages = pkgs.buildEnv {
            name = "my-packages";
            paths = [
                #sage
                #sageWithDoc
                #texlive.combined.scheme-medium
                #pandoc
                #localsend
                #python2
                #trashy
            ];
            pathsToLink = [ "/share/man" "/share/doc" "/bin" ];
            extraOutputsToInstall = [ "man" "doc" ];
        };
    };
}

# to install packages run `nix-env -iA nixpkgs.myPackages`
