local states = {
    ["noloop"] = 0,
    ["a"] = 1,
    ["b"] = 2
}

local state = states.noloop
local filename
local pwd
local start_time
local end_time

local function on_file_loaded()
    filename = mp.get_property("filename")
    pwd = mp.get_property("working-directory")
end

local function set_ab_loop()
    local success, err = mp.command("ab-loop")
    if not success then
        mp.osd_message("Error setting AB loop: " .. (err or "unknown error"))
        start_time = nil
        end_time = nil
    else
        state = (state + 1) % 3
        if state == states.a then
            start_time = mp.get_property("time-pos/full")
        elseif state == states.b then
            end_time = mp.get_property("time-pos/full")
        elseif state == states.noloop then
            mp.osd_message("Didn't save; AB loop cleared", 1)
            start_time = nil
            end_time = nil
        end
    end
end

local function write_to_file()
    if state ~= states.b then
        mp.osd_message("AB points aren't set")
        return nil
    end

    local f, err = io.open(pwd .. "/cutlist-ffmpeg.txt", "a+")
    if not f then
        mp.osd_message("Error creating/opening file")
        return nil
    end

    local success, err = mp.command("ab-loop")
    if not success then
        mp.osd_message("Error writing AB loop: " .. (err or "unknown error"))
    else
        state = (state + 1) % 3
        -- mp.osd_message(filename .. " :\n " .. start_time .. " " .. end_time, 5)
        f:write(string.format("%s/%s/%s\n", filename, start_time, end_time))
    end
    start_time = nil
    end_time = nil
end

mp.register_event("file-loaded", on_file_loaded)
mp.add_key_binding("l", set_ab_loop)
mp.add_key_binding("enter", write_to_file)
