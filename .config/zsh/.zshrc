# Not sure FIXME
# the typeset -A command is used to declare and define associative arrays
# This line is likely obsolete
# typeset -A key

stty stop undef # disable ctrl-s to freeze terminal

#new options
setopt APPEND_HISTORY
setopt SHARE_HISTORY
setopt INC_APPEND_HISTORY
setopt HIST_IGNORE_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
setopt HIST_REDUCE_BLANKS
# History
setopt HIST_IGNORE_SPACE
setopt EXTENDED_HISTORY
HISTSIZE=100000
SAVEHIST=100000
HISTFILE=~/.cache/zsh/history

# Load aliases and shortcuts for files
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/aliasrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/aliasrc"
# [ -f "${XDG_CONFIG_HOME:-$HOME/.config}/aliasrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/filesrc" # TODO

# Completion FIXME
zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle :compinstall filename '/home/jokerj40/.zshrc'

# Basic auto/tab complete
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots) # for hidden files

# Plugins
fpath=(~/.config/zsh/plugins $fpath)

# VI mode
bindkey -v
export KEYTIMEOUT=1

# Backwards search
bindkey '^R' history-incremental-pattern-search-backward

# Fix movement in vi mode FIXME fix the fix
bindkey  "^[[H"   beginning-of-line
bindkey  "^[[F"   end-of-line
bindkey  "^[[3~"  delete-char

# Use vim keys in tab complete menu
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# CHANGE '\e[3 q' to '\e[6 q' to make cursor look like an l-shaped beam
# Change cursor shape for different vi modes
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'underline' ]]; then
    echo -ne '\e[6 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[6 q"
}
zle -N zle-line-init
echo -ne '\e[6 q' # use underline shape cursor on startup
preexec() { echo -ne '\e[6 q' ;} # use underline shape cursor for each new prompt

# Edit line in vim with ctrl-e
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# Coloring manpages
export LESS_TERMCAP_mb=$'\e[6m'          # begin blinking
export LESS_TERMCAP_md=$'\e[34m'         # begin bold
export LESS_TERMCAP_us=$'\e[4;32m'       # begin underline
export LESS_TERMCAP_so=$'\e[1;33;41m'    # begin standout-mode - info box
export LESS_TERMCAP_me=$'\e[m'           # end mode
export LESS_TERMCAP_ue=$'\e[m'           # end underline
export LESS_TERMCAP_se=$'\e[m'           # end standout-mode

# TODO add support for in-terminal filemanager like ranger or vifm
# Move completion dump file into cache folder FIXME

# Pretty prompt
hands_free()
{
  echo -e "\u276F"
}

NEWLINE=$'\n'
# PROMPT="$(date +"%H:%M")${NEWLINE}%2~ "

if [[ $UID == 0 ]]; then
  PROMPT="%B%F{blue}root%b%F{white} at %2~ %B%F{red}# "
else
  PROMPT="%B%F{red}[%b%F{white}%2~%B%F{red}]%b%F{white}${NEWLINE}-> "
fi

# Enable highlighting. Must be last
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
